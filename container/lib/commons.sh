#!/usr/bin/env bash

export KEYBASE_ALLOW_ROOT=1
export KEYBASE_RUN_MODE=prod

export keybase_start_marker="/tmp/start-keybase-marker"
export keybase_credentials_file="/tmp/keybase-credentials"
export keybase_fs_log="${HOME}/.cache/keybase/keybase.kbfs.log"
export keybase_container_log="/var/log/keybase-container.log"
