#!/usr/bin/env bash

source /opt/keybase/lib/commons.sh

# wait for file to bre created
while [[ ! -f ${keybase_fs_log} ]]; do
     echo "==> Waiting for Keybase FS to start"
     sleep 1;
done

# wait for keybase fs to be initialized
while ! grep "Created new folder-branch" ${keybase_fs_log} 2> /dev/null; do
    echo "==> Waiting for Keybase FS to be ready"
    sleep 1;
done
