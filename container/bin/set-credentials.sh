#!/usr/bin/env bash

source /opt/keybase/lib/commons.sh

echo "export KEYBASE_USERNAME=\"${1}\"" >> ${keybase_credentials_file}
echo "export KEYBASE_PAPERKEY=\"${2}\"" >> ${keybase_credentials_file}
touch ${keybase_start_marker}

# wait for keybase mount point to be ready (at most 60 secs)
timeout 60 bash -c /opt/keybase/bin/wait-for-fs-ready.sh
