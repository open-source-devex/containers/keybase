#!/usr/bin/env bash

source /opt/keybase/lib/commons.sh

if [[ -z "${KEYBASE_USERNAME}" || -z "${KEYBASE_PAPERKEY}" ]]; then
    source ${keybase_credentials_file}
fi

mkdir /keybase
mkdir -p ${HOME}/.cache/keybase

echo "==> Starting keybase client"
keybase oneshot
sleep 2

echo "==> Starting keybase FS"
kbfsfuse -debug -log-to-file /keybase &
