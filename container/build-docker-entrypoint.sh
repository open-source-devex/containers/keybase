#!/bin/sh

set -e

if [[ "${DEBUG_ENTRYPOINT}" = "true" ]]; then
    set -x
fi

ARG=$1

if [[ -n "${KEYBASE_PAPERKEY}" ]]; then
    if [[ -d /keybase ]]; then

        echo "==> Keybase seems to be initialized, skipping"

    elif [[ -z "${KEYBASE_USERNAME}" ]]; then

        echo "Please provide the variables KEYBASE_USERNAME and KEYBASE_PAPERKEY."
        echo "if you need to run this without starting Keybase do unset KEYBASE_PAPERKEY in the container environment"
        exit 1

    else

        /opt/keybase/bin/start-client.sh
        # wait for keybase mount point to be ready (at most 40 secs)
        timeout 40 bash -c /opt/keybase/bin/wait-for-fs-ready.sh
    fi

else

    echo "==> Skipping keybase start"

fi

if [[ -z "${ARG}" ]]; then

  exec /bin/sh

else

  exec "$@"

fi
