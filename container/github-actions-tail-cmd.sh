#!/usr/bin/env bash

source /opt/keybase/lib/commons.sh

touch /var/log/keybase-container.log
echo "==> Started container $(date)" | tee -a ${keybase_container_log}

# wait for secret to be available from an action
while [[ ! -f ${keybase_start_marker} ]]; do
     echo "==> Waiting for start keybase credentials to be supplied" | tee -a  ${keybase_container_log}
     sleep 1
done

# Load credentials
source ${keybase_credentials_file}

# trigger keybase to start
/opt/keybase/bin/start-client.sh | tee -a  ${keybase_container_log}

echo "==> Keybase started" | tee -a  ${keybase_container_log}

# keep the container running
/usr/bin/tail -f /dev/null
