[![pipeline status](https://gitlab.com/open-source-devex/containers/build/badges/master/pipeline.svg)](https://gitlab.com/open-source-devex/containers/build/commits/master)

# Docker container that provides access to Keybase volumes

Based on `alpine:latest` for accessing volumes that will be mounted in `/keybase`

The container supports parameters:

| Variable         | Default    | Usage   |
|------------------|---------|---------------|
| KEYBASE_USERNAME | none  | The username |
| KEYBASE_PAPERKEY | none  | Create a "recovery device" paper key this will be used to access. The container will only try to start keybase if this variable is defined (but KEYBASE_USERNAME is also required to login to keybase)  |
| DEBUG_ENTRYPOINT | false | Set to "true" to  of the entrypoint script |

When KEYBASE_PAPERKEY is defined the container will look for KEYBASE_USERNAME to login to keybase. Logging in includes waiting for Keybase FS mounting to finish.
There is a script `/opt/keybase/bin/wait-for-fs-ready.sh` that can be called independently to verify that Kaybase FS is running.

If you write to files in the volumes. Keybase needs a little bit of time to flush the writes (generally <1 second).
The container provides a script that checks all content is flushed. To use it call it with the path of the file that was written to.
```bash
/opt/keybase/bin/wait-for-flush.sh "/keybase/team/folder-last-written-to/"
```

## Using container for GitLab-CI

This container defines an entrypoint script that starts keybase and then passes control to `sh`, which is how GitLab-CI expects to run the container.
Building on top of this container is as simple as add `FROM registry.gitlab.com/open-source-devex/containers/keybase:latest` to your Dockerfile and then extending the image.
See https://gitlab.com/open-source-devex/containers/build for an example of how to extend this container image.

## Using container for GitHub Actions

There are two ways of using containers in GiHub Actions jobs. One is to set the container at the job level, the other is to set the container at a job step.
The current version of the container was only tested at the job level.

### Setting container at the job level

When setting the container at the job level, one cannot inject secrets into the container environment because neither the secrets context (ie. `{{ secrets.xyz }}`) not variable substitution seem to work there.
Another limitation of setting the container at the job level is that GitHub Actions always starts the container by overriding the default entrypoint to `tail` (with args `["-f", "/dev/null"]`).
Therefore, in the container we set an executable script with the name `tail` in the env path, so that we can still run some initialization logic when the container starts.
However, because at initialization time we cannot inject secrets, the initialization logic for GitHub Actions enters into a loop that is broken when a specific marker file is created.
That marker file is created by a call to `/opt/keybase/bin/set-credentials.sh "username" "paper key"`.

Here's an example of a GitHub Actions workflow that uses GitHub Secrets and this container.
```yaml
name: CI

on: [push]

jobs:
  build:
    env:
      KB_USER: ${{ secrets.user }}
      KB_PASS: ${{ secrets.paperkey }}
    runs-on: ubuntu-latest
    container:
      image: 'registry.gitlab.com/open-source-devex/containers/keybase:github-actions'
      options: --privileged --device /dev/fuse
      env:
        DEBUG_ENTRYPOINT: true
    steps:
      - run: /opt/keybase/bin/set-credentials.sh "${KB_USER}" "${KB_PASS}"
      - run: cat /keybase/private/builduser/test.txt # access a file on Keybase FS
      - run: /opt/keybase/bin/wait-for-flush.sh /keybase # Not necessary since no file was updated but still a best practice to always do
        if: always()
      - run: /opt/keybase/bin/stop-client.sh
        if: always()
# Uncomment to print out the container init logs
#      - run: timeout 40 bash -c "tail -f /var/log/keybase-container.log" || true
#        if: always()
```

## Building new containers without code changes

Because every commit to master in this repository creates a release that tagged in git, the best way to trigger a new container build without the code having changed is to create an empty commit.
Such builds are needed for example when the base container has been updated.

```bash
git commit --allow-empty -m "Trigger build of new container on latest base image"
```
